#ifndef NOTIC_H
#define NOTIC_H

#include <QWidget>

namespace Ui {
class notic;
}

class notic : public QWidget
{
    Q_OBJECT

public:
    explicit notic(QWidget *parent = nullptr);
    ~notic();

private:
    Ui::notic *ui;
};

#endif // NOTIC_H
