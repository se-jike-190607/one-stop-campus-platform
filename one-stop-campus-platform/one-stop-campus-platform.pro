QT += core gui


QT += sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    assignment.cpp \
    base_class.cpp \
    base_model.cpp \
    form.cpp \
    inform.cpp \
    list1.cpp \
    list_work.cpp \
    main.cpp \
    mainwindow.cpp \
    mysql.cpp \
    notic.cpp \
    text.cpp

HEADERS += \
    assignment.h \
    base_class.h \
    base_model.h \
    form.h \
    inform.h \
    list1.h \
    list_work.h \
    mainwindow.h \
    mysql.h \
    notic.h \
    text.h

FORMS += \
    assignment.ui \
    form.ui \
    inform.ui \
    list1.ui \
    list_work.ui \
    mainwindow.ui \
    notic.ui \
    text.ui



# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resource.qrc
