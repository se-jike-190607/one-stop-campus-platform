#include "mysql.h"


Mysql::Mysql()
{

    db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("127.0.0.1");
    db.setPort(3306);
    db.setDatabaseName("campus_platform");
    db.setUserName("root");
    db.setPassword("root");
    bool ok = db.open();
    if (ok){

        //此处连接成功提醒———————项目完成后删除
        QMessageBox::information(nullptr, "infor", "success");

        /*
        //此处测试
        QSqlQuery query("select * from user",db);
        QSqlRecord rec = query.record();
        while(query.next())
        {
            rec = query.record();
            int snamecol = rec.indexOf("name");
            int spass=rec.indexOf("password");
            QString value = query.value(snamecol).toString();
            qDebug()<<"sname:"<<value;
            qDebug()<<"pass:"<<query.value(spass).toString();
        }
        */


    }
    else {

        //此处连接失败提醒———————项目进行中更改处理方式
        QMessageBox::information(nullptr, "infor", "open failed");
        qDebug()<<"error open database because"<<db.lastError().text();
    }
}

Mysql::~Mysql()
{
    db.close();
}

user* Mysql::check_id_password(QString id,QString pass)
{

    QSqlQuery query;
    query.prepare("select * from user where id = :id and password = :pass");
    query.bindValue(":id",id);   // 绑定数据到指定的位置
    query.bindValue(":pass",pass);
    query.exec();
    QSqlRecord rec = query.record();
    while(query.next())
    {
        user * users=new user(query);
        return users;
    }
    return nullptr;
}
