#ifndef ASSIGNMENT_H
#define ASSIGNMENT_H

#include <QWidget>

namespace Ui {
class assignment;
}

class assignment : public QWidget
{
    Q_OBJECT

public:
    explicit assignment(QWidget *parent = nullptr);
    ~assignment();

private:
    Ui::assignment *ui;
};

#endif // ASSIGNMENT_H
