#include "base_class.h"

user::user(QSqlQuery query)
{
    this->id=query.value(query.record().indexOf("id")).toString();
    this->id_number=query.value(query.record().indexOf("id_number")).toString();
    this->password=query.value(query.record().indexOf("password")).toString();
    this->name=query.value(query.record().indexOf("name")).toString();
    this->phone_number=query.value(query.record().indexOf("phone_number")).toString();
    this->value=query.value(query.record().indexOf("value")).toInt();
    this->sex=query.value(query.record().indexOf("sex")).toBool();
    this->time_in_job=query.value(query.record().indexOf("time_in_job")).toDate();
}
