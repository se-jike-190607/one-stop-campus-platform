#ifndef LIST1_H
#define LIST1_H

#include <QWidget>

namespace Ui {
class list1;
}

class list1 : public QWidget
{
    Q_OBJECT

public:
    explicit list1(QWidget *parent = nullptr);
    ~list1();

private:
    Ui::list1 *ui;
};

#endif // LIST1_H
