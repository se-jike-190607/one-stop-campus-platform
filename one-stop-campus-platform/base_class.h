#ifndef BASE_CLASS_H
#define BASE_CLASS_H


#include<QString>
#include<QDate>
#include <QSqlQuery>
#include <QSqlRecord>

//此处为基础类型库

//用户类
class user
{
public:
    QString id;
    QString password;
    int value;          //权限  10 学生
    QString name;
    QString id_number;  //身份证号
    QString phone_number; //手机号
    bool sex;           //性别
    QDate time_in_job;  //入学时间

    user();
    user(QSqlQuery query);

};

//课程类
class course
{
public:
    QString id;
    QString name;
    QDate time_in_job;  //开课时间
    QString teacher_id; //教师id
    int flag;           //标记 上课时间

};

//资源\作业
class assignment
{
public:
    QString id;
    QString name;
    int value;          //标记
    QString content;    //内容
    QString course_id;  //所属课程id

};

//成绩
class score
{
public:
    QString student_id; //学生id
    QString course_id;  //课程id
    int common_score;   //平时成绩
    int end_score;      //期末成绩

};


//公告 通知
class announcement
{
public:
    QString id;
    QString admin_id; //管理员id
    QString content;    //内容
    QString accessory;    //附件链接

};


//抄送名单
class ann_list
{
public:
    QString id;//通知id
    QString user_id; //抄送id
    bool result;     //结果

};



#endif // BASE_CLASS_H
