#ifndef FORM_H
#define FORM_H

#include <QWidget>
#include <QObject>
#include <QListWidget>
#include "mysql.h"

namespace Ui {
class Form;
}

class Form : public QWidget
{
    Q_OBJECT

public:
    explicit Form(QWidget *parent = nullptr);
    ~Form();

    Mysql *mysql;
    user * users;

private slots:
    void on_buttonBox_accepted();

    void on_pushButton_clicked();

    void ceshi();

    void on_listWidget_itemClicked(QListWidgetItem *item);

private:
    Ui::Form *ui;
};

#endif // FORM_H
