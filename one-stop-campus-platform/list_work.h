#ifndef LIST_WORK_H
#define LIST_WORK_H

#include <QWidget>

namespace Ui {
class list_work;
}

class list_work : public QWidget
{
    Q_OBJECT

public:
    explicit list_work(QWidget *parent = nullptr);
    ~list_work();

private:
    Ui::list_work *ui;
};

#endif // LIST_WORK_H
