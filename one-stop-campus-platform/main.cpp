#include "mainwindow.h"
#include "form.h"
#include "mysql.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    /*
    MainWindow w;
    w.show();
    */
    Mysql mysql;

    Form form;
    form.mysql=&mysql;
    form.show();
    return a.exec();
}
