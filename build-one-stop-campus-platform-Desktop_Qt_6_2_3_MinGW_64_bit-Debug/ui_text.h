/********************************************************************************
** Form generated from reading UI file 'text.ui'
**
** Created by: Qt User Interface Compiler version 6.2.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TEXT_H
#define UI_TEXT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_text
{
public:
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton;
    QPushButton *pushButton_4;
    QSpacerItem *verticalSpacer;
    QListView *listView_2;
    QLabel *label;
    QStackedWidget *stackedWidget;
    QWidget *page;
    QLineEdit *lineEdit;
    QLineEdit *lineEdit_2;
    QLineEdit *lineEdit_3;
    QWidget *page_3;
    QFrame *line_2;
    QPushButton *pushButton_6;
    QTextBrowser *textBrowser_2;
    QLabel *label_4;
    QLabel *label_5;
    QComboBox *comboBox_2;
    QTableWidget *tableWidget;
    QWidget *page_4;
    QLabel *label_6;
    QFrame *line_3;
    QLabel *label_7;
    QComboBox *comboBox_3;
    QPushButton *pushButton_7;
    QTableWidget *tableWidget_2;
    QWidget *page_2;
    QLabel *label_8;
    QComboBox *comboBox_4;
    QLabel *label_2;
    QFrame *line;
    QLabel *label_3;
    QComboBox *comboBox_5;
    QPushButton *pushButton_5;
    QTableWidget *tableWidget_3;

    void setupUi(QWidget *text)
    {
        if (text->objectName().isEmpty())
            text->setObjectName(QString::fromUtf8("text"));
        text->resize(1258, 579);
        verticalLayoutWidget = new QWidget(text);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(0, 80, 191, 471));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        pushButton_2 = new QPushButton(verticalLayoutWidget);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        verticalLayout->addWidget(pushButton_2);

        pushButton_3 = new QPushButton(verticalLayoutWidget);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));

        verticalLayout->addWidget(pushButton_3);

        pushButton = new QPushButton(verticalLayoutWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        verticalLayout->addWidget(pushButton);

        pushButton_4 = new QPushButton(verticalLayoutWidget);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));

        verticalLayout->addWidget(pushButton_4);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        listView_2 = new QListView(text);
        listView_2->setObjectName(QString::fromUtf8("listView_2"));
        listView_2->setGeometry(QRect(0, 0, 1261, 61));
        label = new QLabel(text);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(30, 10, 1221, 31));
        label->setLineWidth(1);
        stackedWidget = new QStackedWidget(text);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setGeometry(QRect(230, 90, 991, 451));
        page = new QWidget();
        page->setObjectName(QString::fromUtf8("page"));
        lineEdit = new QLineEdit(page);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setGeometry(QRect(10, 10, 113, 27));
        lineEdit_2 = new QLineEdit(page);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(10, 50, 113, 27));
        lineEdit_3 = new QLineEdit(page);
        lineEdit_3->setObjectName(QString::fromUtf8("lineEdit_3"));
        lineEdit_3->setGeometry(QRect(10, 90, 113, 27));
        stackedWidget->addWidget(page);
        page_3 = new QWidget();
        page_3->setObjectName(QString::fromUtf8("page_3"));
        line_2 = new QFrame(page_3);
        line_2->setObjectName(QString::fromUtf8("line_2"));
        line_2->setGeometry(QRect(220, 30, 561, 20));
        line_2->setFrameShape(QFrame::HLine);
        line_2->setFrameShadow(QFrame::Sunken);
        pushButton_6 = new QPushButton(page_3);
        pushButton_6->setObjectName(QString::fromUtf8("pushButton_6"));
        pushButton_6->setGeometry(QRect(660, 50, 92, 29));
        textBrowser_2 = new QTextBrowser(page_3);
        textBrowser_2->setObjectName(QString::fromUtf8("textBrowser_2"));
        textBrowser_2->setGeometry(QRect(540, 50, 81, 31));
        label_4 = new QLabel(page_3);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(440, 10, 69, 20));
        label_5 = new QLabel(page_3);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(230, 60, 69, 20));
        comboBox_2 = new QComboBox(page_3);
        comboBox_2->addItem(QString());
        comboBox_2->addItem(QString());
        comboBox_2->addItem(QString());
        comboBox_2->setObjectName(QString::fromUtf8("comboBox_2"));
        comboBox_2->setGeometry(QRect(300, 50, 201, 28));
        tableWidget = new QTableWidget(page_3);
        if (tableWidget->columnCount() < 7)
            tableWidget->setColumnCount(7);
        QFont font;
        font.setPointSize(5);
        QTableWidgetItem *__qtablewidgetitem = new QTableWidgetItem();
        __qtablewidgetitem->setFont(font);
        tableWidget->setHorizontalHeaderItem(0, __qtablewidgetitem);
        QTableWidgetItem *__qtablewidgetitem1 = new QTableWidgetItem();
        __qtablewidgetitem1->setFont(font);
        tableWidget->setHorizontalHeaderItem(1, __qtablewidgetitem1);
        QTableWidgetItem *__qtablewidgetitem2 = new QTableWidgetItem();
        __qtablewidgetitem2->setFont(font);
        tableWidget->setHorizontalHeaderItem(2, __qtablewidgetitem2);
        QTableWidgetItem *__qtablewidgetitem3 = new QTableWidgetItem();
        __qtablewidgetitem3->setFont(font);
        tableWidget->setHorizontalHeaderItem(3, __qtablewidgetitem3);
        QTableWidgetItem *__qtablewidgetitem4 = new QTableWidgetItem();
        __qtablewidgetitem4->setFont(font);
        tableWidget->setHorizontalHeaderItem(4, __qtablewidgetitem4);
        QTableWidgetItem *__qtablewidgetitem5 = new QTableWidgetItem();
        __qtablewidgetitem5->setFont(font);
        tableWidget->setHorizontalHeaderItem(5, __qtablewidgetitem5);
        QTableWidgetItem *__qtablewidgetitem6 = new QTableWidgetItem();
        __qtablewidgetitem6->setFont(font);
        tableWidget->setHorizontalHeaderItem(6, __qtablewidgetitem6);
        if (tableWidget->rowCount() < 4)
            tableWidget->setRowCount(4);
        QTableWidgetItem *__qtablewidgetitem7 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(0, __qtablewidgetitem7);
        QTableWidgetItem *__qtablewidgetitem8 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(1, __qtablewidgetitem8);
        QTableWidgetItem *__qtablewidgetitem9 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(2, __qtablewidgetitem9);
        QTableWidgetItem *__qtablewidgetitem10 = new QTableWidgetItem();
        tableWidget->setVerticalHeaderItem(3, __qtablewidgetitem10);
        QTableWidgetItem *__qtablewidgetitem11 = new QTableWidgetItem();
        tableWidget->setItem(0, 0, __qtablewidgetitem11);
        QTableWidgetItem *__qtablewidgetitem12 = new QTableWidgetItem();
        tableWidget->setItem(0, 1, __qtablewidgetitem12);
        QTableWidgetItem *__qtablewidgetitem13 = new QTableWidgetItem();
        tableWidget->setItem(0, 2, __qtablewidgetitem13);
        QTableWidgetItem *__qtablewidgetitem14 = new QTableWidgetItem();
        tableWidget->setItem(0, 3, __qtablewidgetitem14);
        QTableWidgetItem *__qtablewidgetitem15 = new QTableWidgetItem();
        tableWidget->setItem(0, 4, __qtablewidgetitem15);
        QTableWidgetItem *__qtablewidgetitem16 = new QTableWidgetItem();
        tableWidget->setItem(1, 0, __qtablewidgetitem16);
        QTableWidgetItem *__qtablewidgetitem17 = new QTableWidgetItem();
        tableWidget->setItem(1, 2, __qtablewidgetitem17);
        QTableWidgetItem *__qtablewidgetitem18 = new QTableWidgetItem();
        tableWidget->setItem(1, 3, __qtablewidgetitem18);
        QTableWidgetItem *__qtablewidgetitem19 = new QTableWidgetItem();
        tableWidget->setItem(2, 0, __qtablewidgetitem19);
        QTableWidgetItem *__qtablewidgetitem20 = new QTableWidgetItem();
        tableWidget->setItem(2, 3, __qtablewidgetitem20);
        QTableWidgetItem *__qtablewidgetitem21 = new QTableWidgetItem();
        tableWidget->setItem(3, 1, __qtablewidgetitem21);
        QTableWidgetItem *__qtablewidgetitem22 = new QTableWidgetItem();
        tableWidget->setItem(3, 2, __qtablewidgetitem22);
        tableWidget->setObjectName(QString::fromUtf8("tableWidget"));
        tableWidget->setGeometry(QRect(70, 130, 891, 181));
        stackedWidget->addWidget(page_3);
        page_4 = new QWidget();
        page_4->setObjectName(QString::fromUtf8("page_4"));
        label_6 = new QLabel(page_4);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(450, 10, 69, 20));
        line_3 = new QFrame(page_4);
        line_3->setObjectName(QString::fromUtf8("line_3"));
        line_3->setGeometry(QRect(230, 30, 421, 20));
        line_3->setFrameShape(QFrame::HLine);
        line_3->setFrameShadow(QFrame::Sunken);
        label_7 = new QLabel(page_4);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(230, 60, 69, 20));
        comboBox_3 = new QComboBox(page_4);
        comboBox_3->addItem(QString());
        comboBox_3->addItem(QString());
        comboBox_3->addItem(QString());
        comboBox_3->setObjectName(QString::fromUtf8("comboBox_3"));
        comboBox_3->setGeometry(QRect(310, 50, 201, 28));
        pushButton_7 = new QPushButton(page_4);
        pushButton_7->setObjectName(QString::fromUtf8("pushButton_7"));
        pushButton_7->setGeometry(QRect(560, 50, 92, 29));
        tableWidget_2 = new QTableWidget(page_4);
        if (tableWidget_2->columnCount() < 6)
            tableWidget_2->setColumnCount(6);
        QTableWidgetItem *__qtablewidgetitem23 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(0, __qtablewidgetitem23);
        QTableWidgetItem *__qtablewidgetitem24 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(1, __qtablewidgetitem24);
        QTableWidgetItem *__qtablewidgetitem25 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(2, __qtablewidgetitem25);
        QTableWidgetItem *__qtablewidgetitem26 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(3, __qtablewidgetitem26);
        QTableWidgetItem *__qtablewidgetitem27 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(4, __qtablewidgetitem27);
        QTableWidgetItem *__qtablewidgetitem28 = new QTableWidgetItem();
        tableWidget_2->setHorizontalHeaderItem(5, __qtablewidgetitem28);
        if (tableWidget_2->rowCount() < 4)
            tableWidget_2->setRowCount(4);
        QTableWidgetItem *__qtablewidgetitem29 = new QTableWidgetItem();
        tableWidget_2->setVerticalHeaderItem(0, __qtablewidgetitem29);
        QTableWidgetItem *__qtablewidgetitem30 = new QTableWidgetItem();
        tableWidget_2->setVerticalHeaderItem(1, __qtablewidgetitem30);
        QTableWidgetItem *__qtablewidgetitem31 = new QTableWidgetItem();
        tableWidget_2->setVerticalHeaderItem(2, __qtablewidgetitem31);
        QTableWidgetItem *__qtablewidgetitem32 = new QTableWidgetItem();
        tableWidget_2->setVerticalHeaderItem(3, __qtablewidgetitem32);
        QTableWidgetItem *__qtablewidgetitem33 = new QTableWidgetItem();
        tableWidget_2->setItem(0, 0, __qtablewidgetitem33);
        QTableWidgetItem *__qtablewidgetitem34 = new QTableWidgetItem();
        __qtablewidgetitem34->setTextAlignment(Qt::AlignCenter);
        tableWidget_2->setItem(0, 1, __qtablewidgetitem34);
        QTableWidgetItem *__qtablewidgetitem35 = new QTableWidgetItem();
        tableWidget_2->setItem(0, 2, __qtablewidgetitem35);
        QTableWidgetItem *__qtablewidgetitem36 = new QTableWidgetItem();
        __qtablewidgetitem36->setTextAlignment(Qt::AlignCenter);
        tableWidget_2->setItem(0, 3, __qtablewidgetitem36);
        QTableWidgetItem *__qtablewidgetitem37 = new QTableWidgetItem();
        __qtablewidgetitem37->setTextAlignment(Qt::AlignCenter);
        tableWidget_2->setItem(0, 4, __qtablewidgetitem37);
        QTableWidgetItem *__qtablewidgetitem38 = new QTableWidgetItem();
        tableWidget_2->setItem(1, 0, __qtablewidgetitem38);
        QTableWidgetItem *__qtablewidgetitem39 = new QTableWidgetItem();
        __qtablewidgetitem39->setTextAlignment(Qt::AlignCenter);
        tableWidget_2->setItem(1, 1, __qtablewidgetitem39);
        QTableWidgetItem *__qtablewidgetitem40 = new QTableWidgetItem();
        tableWidget_2->setItem(1, 2, __qtablewidgetitem40);
        QTableWidgetItem *__qtablewidgetitem41 = new QTableWidgetItem();
        __qtablewidgetitem41->setTextAlignment(Qt::AlignCenter);
        tableWidget_2->setItem(1, 3, __qtablewidgetitem41);
        QTableWidgetItem *__qtablewidgetitem42 = new QTableWidgetItem();
        __qtablewidgetitem42->setTextAlignment(Qt::AlignCenter);
        tableWidget_2->setItem(1, 4, __qtablewidgetitem42);
        QTableWidgetItem *__qtablewidgetitem43 = new QTableWidgetItem();
        tableWidget_2->setItem(2, 0, __qtablewidgetitem43);
        QTableWidgetItem *__qtablewidgetitem44 = new QTableWidgetItem();
        __qtablewidgetitem44->setTextAlignment(Qt::AlignCenter);
        tableWidget_2->setItem(2, 1, __qtablewidgetitem44);
        QTableWidgetItem *__qtablewidgetitem45 = new QTableWidgetItem();
        tableWidget_2->setItem(2, 2, __qtablewidgetitem45);
        QTableWidgetItem *__qtablewidgetitem46 = new QTableWidgetItem();
        __qtablewidgetitem46->setTextAlignment(Qt::AlignCenter);
        tableWidget_2->setItem(2, 3, __qtablewidgetitem46);
        QTableWidgetItem *__qtablewidgetitem47 = new QTableWidgetItem();
        __qtablewidgetitem47->setTextAlignment(Qt::AlignCenter);
        tableWidget_2->setItem(2, 4, __qtablewidgetitem47);
        QTableWidgetItem *__qtablewidgetitem48 = new QTableWidgetItem();
        tableWidget_2->setItem(3, 0, __qtablewidgetitem48);
        QTableWidgetItem *__qtablewidgetitem49 = new QTableWidgetItem();
        __qtablewidgetitem49->setTextAlignment(Qt::AlignCenter);
        tableWidget_2->setItem(3, 1, __qtablewidgetitem49);
        QTableWidgetItem *__qtablewidgetitem50 = new QTableWidgetItem();
        tableWidget_2->setItem(3, 2, __qtablewidgetitem50);
        QTableWidgetItem *__qtablewidgetitem51 = new QTableWidgetItem();
        __qtablewidgetitem51->setTextAlignment(Qt::AlignCenter);
        tableWidget_2->setItem(3, 3, __qtablewidgetitem51);
        QTableWidgetItem *__qtablewidgetitem52 = new QTableWidgetItem();
        __qtablewidgetitem52->setTextAlignment(Qt::AlignCenter);
        tableWidget_2->setItem(3, 4, __qtablewidgetitem52);
        tableWidget_2->setObjectName(QString::fromUtf8("tableWidget_2"));
        tableWidget_2->setGeometry(QRect(100, 140, 761, 191));
        stackedWidget->addWidget(page_4);
        page_2 = new QWidget();
        page_2->setObjectName(QString::fromUtf8("page_2"));
        label_8 = new QLabel(page_2);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(190, 70, 69, 20));
        comboBox_4 = new QComboBox(page_2);
        comboBox_4->addItem(QString());
        comboBox_4->addItem(QString());
        comboBox_4->addItem(QString());
        comboBox_4->setObjectName(QString::fromUtf8("comboBox_4"));
        comboBox_4->setGeometry(QRect(260, 70, 201, 28));
        label_2 = new QLabel(page_2);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(430, 20, 69, 20));
        line = new QFrame(page_2);
        line->setObjectName(QString::fromUtf8("line"));
        line->setGeometry(QRect(190, 50, 551, 20));
        line->setFrameShape(QFrame::HLine);
        line->setFrameShadow(QFrame::Sunken);
        label_3 = new QLabel(page_2);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(490, 70, 69, 20));
        comboBox_5 = new QComboBox(page_2);
        comboBox_5->addItem(QString());
        comboBox_5->addItem(QString());
        comboBox_5->addItem(QString());
        comboBox_5->setObjectName(QString::fromUtf8("comboBox_5"));
        comboBox_5->setGeometry(QRect(560, 70, 61, 28));
        pushButton_5 = new QPushButton(page_2);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));
        pushButton_5->setGeometry(QRect(650, 70, 92, 29));
        tableWidget_3 = new QTableWidget(page_2);
        if (tableWidget_3->columnCount() < 4)
            tableWidget_3->setColumnCount(4);
        QTableWidgetItem *__qtablewidgetitem53 = new QTableWidgetItem();
        tableWidget_3->setHorizontalHeaderItem(0, __qtablewidgetitem53);
        QTableWidgetItem *__qtablewidgetitem54 = new QTableWidgetItem();
        tableWidget_3->setHorizontalHeaderItem(1, __qtablewidgetitem54);
        QTableWidgetItem *__qtablewidgetitem55 = new QTableWidgetItem();
        tableWidget_3->setHorizontalHeaderItem(2, __qtablewidgetitem55);
        QTableWidgetItem *__qtablewidgetitem56 = new QTableWidgetItem();
        tableWidget_3->setHorizontalHeaderItem(3, __qtablewidgetitem56);
        if (tableWidget_3->rowCount() < 4)
            tableWidget_3->setRowCount(4);
        QTableWidgetItem *__qtablewidgetitem57 = new QTableWidgetItem();
        tableWidget_3->setVerticalHeaderItem(0, __qtablewidgetitem57);
        QTableWidgetItem *__qtablewidgetitem58 = new QTableWidgetItem();
        tableWidget_3->setVerticalHeaderItem(1, __qtablewidgetitem58);
        QTableWidgetItem *__qtablewidgetitem59 = new QTableWidgetItem();
        tableWidget_3->setVerticalHeaderItem(2, __qtablewidgetitem59);
        QTableWidgetItem *__qtablewidgetitem60 = new QTableWidgetItem();
        tableWidget_3->setVerticalHeaderItem(3, __qtablewidgetitem60);
        QTableWidgetItem *__qtablewidgetitem61 = new QTableWidgetItem();
        tableWidget_3->setItem(0, 0, __qtablewidgetitem61);
        QTableWidgetItem *__qtablewidgetitem62 = new QTableWidgetItem();
        tableWidget_3->setItem(0, 1, __qtablewidgetitem62);
        QTableWidgetItem *__qtablewidgetitem63 = new QTableWidgetItem();
        tableWidget_3->setItem(0, 2, __qtablewidgetitem63);
        QTableWidgetItem *__qtablewidgetitem64 = new QTableWidgetItem();
        tableWidget_3->setItem(0, 3, __qtablewidgetitem64);
        QTableWidgetItem *__qtablewidgetitem65 = new QTableWidgetItem();
        tableWidget_3->setItem(1, 0, __qtablewidgetitem65);
        QTableWidgetItem *__qtablewidgetitem66 = new QTableWidgetItem();
        tableWidget_3->setItem(1, 1, __qtablewidgetitem66);
        QTableWidgetItem *__qtablewidgetitem67 = new QTableWidgetItem();
        tableWidget_3->setItem(1, 2, __qtablewidgetitem67);
        QTableWidgetItem *__qtablewidgetitem68 = new QTableWidgetItem();
        tableWidget_3->setItem(1, 3, __qtablewidgetitem68);
        QTableWidgetItem *__qtablewidgetitem69 = new QTableWidgetItem();
        tableWidget_3->setItem(2, 0, __qtablewidgetitem69);
        QTableWidgetItem *__qtablewidgetitem70 = new QTableWidgetItem();
        tableWidget_3->setItem(2, 1, __qtablewidgetitem70);
        QTableWidgetItem *__qtablewidgetitem71 = new QTableWidgetItem();
        tableWidget_3->setItem(2, 2, __qtablewidgetitem71);
        QTableWidgetItem *__qtablewidgetitem72 = new QTableWidgetItem();
        tableWidget_3->setItem(2, 3, __qtablewidgetitem72);
        QTableWidgetItem *__qtablewidgetitem73 = new QTableWidgetItem();
        tableWidget_3->setItem(3, 0, __qtablewidgetitem73);
        QTableWidgetItem *__qtablewidgetitem74 = new QTableWidgetItem();
        tableWidget_3->setItem(3, 1, __qtablewidgetitem74);
        QTableWidgetItem *__qtablewidgetitem75 = new QTableWidgetItem();
        tableWidget_3->setItem(3, 2, __qtablewidgetitem75);
        QTableWidgetItem *__qtablewidgetitem76 = new QTableWidgetItem();
        tableWidget_3->setItem(3, 3, __qtablewidgetitem76);
        tableWidget_3->setObjectName(QString::fromUtf8("tableWidget_3"));
        tableWidget_3->setGeometry(QRect(170, 140, 601, 241));
        tableWidget_3->horizontalHeader()->setDefaultSectionSize(148);
        tableWidget_3->verticalHeader()->setDefaultSectionSize(48);
        stackedWidget->addWidget(page_2);
        stackedWidget->raise();
        verticalLayoutWidget->raise();
        listView_2->raise();
        label->raise();

        retranslateUi(text);

        stackedWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(text);
    } // setupUi

    void retranslateUi(QWidget *text)
    {
        text->setWindowTitle(QCoreApplication::translate("text", "Form", nullptr));
        pushButton_2->setText(QCoreApplication::translate("text", "\345\255\246\347\224\237\345\255\246\347\261\215", nullptr));
        pushButton_3->setText(QCoreApplication::translate("text", "\346\225\231\345\255\246\345\256\211\346\216\222", nullptr));
        pushButton->setText(QCoreApplication::translate("text", "\345\255\246\347\224\237\346\210\220\347\273\251", nullptr));
        pushButton_4->setText(QCoreApplication::translate("text", "\350\200\203\350\257\225\345\256\211\346\216\222", nullptr));
        label->setText(QCoreApplication::translate("text", "<html><head/><body><p align=\"center\"><span style=\" font-size:16pt; font-weight:700;\">\346\225\231 \345\212\241 \347\263\273 \347\273\237</span></p></body></html>", nullptr));
        lineEdit->setText(QCoreApplication::translate("text", "\345\247\223\345\220\215\357\274\232\347\250\213\346\231\272", nullptr));
        lineEdit_2->setText(QCoreApplication::translate("text", "\346\200\247\345\210\253\357\274\232\347\224\267", nullptr));
        lineEdit_3->setText(QCoreApplication::translate("text", "\345\271\264\351\276\204\357\274\23222", nullptr));
        pushButton_6->setText(QCoreApplication::translate("text", "\346\220\234\347\264\242", nullptr));
        textBrowser_2->setHtml(QCoreApplication::translate("text", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><meta charset=\"utf-8\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Microsoft YaHei UI'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">\345\221\250\346\254\2411-18</p></body></html>", nullptr));
        label_4->setText(QCoreApplication::translate("text", "<html><head/><body><p><span style=\" font-weight:700;\">\346\225\231\345\255\246\345\256\211\346\216\222</span></p></body></html>", nullptr));
        label_5->setText(QCoreApplication::translate("text", "<html><head/><body><p>\345\255\246\345\271\264\345\255\246\346\234\237</p></body></html>", nullptr));
        comboBox_2->setItemText(0, QCoreApplication::translate("text", "2021-2022\345\255\246\345\271\264\347\254\254\344\270\200\345\255\246\346\234\237", nullptr));
        comboBox_2->setItemText(1, QCoreApplication::translate("text", "2021-2022\345\255\246\345\271\264\347\254\254\344\272\214\345\255\246\346\234\237", nullptr));
        comboBox_2->setItemText(2, QCoreApplication::translate("text", "2021-2022\345\255\246\345\271\264\347\254\254\344\270\211\345\255\246\346\234\237", nullptr));

        QTableWidgetItem *___qtablewidgetitem = tableWidget->horizontalHeaderItem(0);
        ___qtablewidgetitem->setText(QCoreApplication::translate("text", "\346\230\237\346\234\237\344\270\200", nullptr));
        QTableWidgetItem *___qtablewidgetitem1 = tableWidget->horizontalHeaderItem(1);
        ___qtablewidgetitem1->setText(QCoreApplication::translate("text", "\346\230\237\346\234\237\344\272\214", nullptr));
        QTableWidgetItem *___qtablewidgetitem2 = tableWidget->horizontalHeaderItem(2);
        ___qtablewidgetitem2->setText(QCoreApplication::translate("text", "\346\230\237\346\234\237\344\270\211", nullptr));
        QTableWidgetItem *___qtablewidgetitem3 = tableWidget->horizontalHeaderItem(3);
        ___qtablewidgetitem3->setText(QCoreApplication::translate("text", "\346\230\237\346\234\237\345\233\233", nullptr));
        QTableWidgetItem *___qtablewidgetitem4 = tableWidget->horizontalHeaderItem(4);
        ___qtablewidgetitem4->setText(QCoreApplication::translate("text", "\346\230\237\346\234\237\344\272\224", nullptr));
        QTableWidgetItem *___qtablewidgetitem5 = tableWidget->horizontalHeaderItem(5);
        ___qtablewidgetitem5->setText(QCoreApplication::translate("text", "\346\230\237\346\234\237\345\205\255", nullptr));
        QTableWidgetItem *___qtablewidgetitem6 = tableWidget->horizontalHeaderItem(6);
        ___qtablewidgetitem6->setText(QCoreApplication::translate("text", "\346\230\237\346\234\237\346\227\245", nullptr));

        const bool __sortingEnabled = tableWidget->isSortingEnabled();
        tableWidget->setSortingEnabled(false);
        QTableWidgetItem *___qtablewidgetitem7 = tableWidget->item(0, 1);
        ___qtablewidgetitem7->setText(QCoreApplication::translate("text", "liunx\347\263\273\347\273\237\347\274\226\347\250\213", nullptr));
        QTableWidgetItem *___qtablewidgetitem8 = tableWidget->item(0, 4);
        ___qtablewidgetitem8->setText(QCoreApplication::translate("text", "\350\275\257\344\273\266\345\267\245\347\250\213", nullptr));
        QTableWidgetItem *___qtablewidgetitem9 = tableWidget->item(1, 0);
        ___qtablewidgetitem9->setText(QCoreApplication::translate("text", "\350\275\257\344\273\266\345\267\245\347\250\213", nullptr));
        QTableWidgetItem *___qtablewidgetitem10 = tableWidget->item(1, 3);
        ___qtablewidgetitem10->setText(QCoreApplication::translate("text", "liunx\347\263\273\347\273\237\347\274\226\347\250\213", nullptr));
        QTableWidgetItem *___qtablewidgetitem11 = tableWidget->item(2, 0);
        ___qtablewidgetitem11->setText(QCoreApplication::translate("text", "C++\347\250\213\345\272\217\350\256\276\350\256\241", nullptr));
        QTableWidgetItem *___qtablewidgetitem12 = tableWidget->item(2, 3);
        ___qtablewidgetitem12->setText(QCoreApplication::translate("text", "\347\225\214\351\235\242\344\272\244\344\272\222", nullptr));
        QTableWidgetItem *___qtablewidgetitem13 = tableWidget->item(3, 1);
        ___qtablewidgetitem13->setText(QCoreApplication::translate("text", "\347\225\214\351\235\242\344\272\244\344\272\222", nullptr));
        QTableWidgetItem *___qtablewidgetitem14 = tableWidget->item(3, 2);
        ___qtablewidgetitem14->setText(QCoreApplication::translate("text", "C++\347\250\213\345\272\217\350\256\276\350\256\241", nullptr));
        tableWidget->setSortingEnabled(__sortingEnabled);

        label_6->setText(QCoreApplication::translate("text", "\345\255\246\347\224\237\346\210\220\347\273\251", nullptr));
        label_7->setText(QCoreApplication::translate("text", "<html><head/><body><p>\345\255\246\345\271\264\345\255\246\346\234\237</p></body></html>", nullptr));
        comboBox_3->setItemText(0, QCoreApplication::translate("text", "2021-2022\345\255\246\345\271\264\347\254\254\344\270\200\345\255\246\346\234\237", nullptr));
        comboBox_3->setItemText(1, QCoreApplication::translate("text", "2021-2022\345\255\246\345\271\264\347\254\254\344\272\214\345\255\246\346\234\237", nullptr));
        comboBox_3->setItemText(2, QCoreApplication::translate("text", "2021-2022\345\255\246\345\271\264\347\254\254\344\270\211\345\255\246\346\234\237", nullptr));

        pushButton_7->setText(QCoreApplication::translate("text", "\346\220\234\347\264\242", nullptr));
        QTableWidgetItem *___qtablewidgetitem15 = tableWidget_2->horizontalHeaderItem(0);
        ___qtablewidgetitem15->setText(QCoreApplication::translate("text", "\350\257\276\347\250\213", nullptr));
        QTableWidgetItem *___qtablewidgetitem16 = tableWidget_2->horizontalHeaderItem(1);
        ___qtablewidgetitem16->setText(QCoreApplication::translate("text", "\345\255\246\345\210\206", nullptr));
        QTableWidgetItem *___qtablewidgetitem17 = tableWidget_2->horizontalHeaderItem(2);
        ___qtablewidgetitem17->setText(QCoreApplication::translate("text", "\347\261\273\345\210\253", nullptr));
        QTableWidgetItem *___qtablewidgetitem18 = tableWidget_2->horizontalHeaderItem(3);
        ___qtablewidgetitem18->setText(QCoreApplication::translate("text", "\346\210\220\347\273\251", nullptr));
        QTableWidgetItem *___qtablewidgetitem19 = tableWidget_2->horizontalHeaderItem(4);
        ___qtablewidgetitem19->setText(QCoreApplication::translate("text", "\345\217\226\345\276\227\345\255\246\345\210\206", nullptr));
        QTableWidgetItem *___qtablewidgetitem20 = tableWidget_2->horizontalHeaderItem(5);
        ___qtablewidgetitem20->setText(QCoreApplication::translate("text", "\345\244\207\346\263\250", nullptr));

        const bool __sortingEnabled1 = tableWidget_2->isSortingEnabled();
        tableWidget_2->setSortingEnabled(false);
        QTableWidgetItem *___qtablewidgetitem21 = tableWidget_2->item(0, 0);
        ___qtablewidgetitem21->setText(QCoreApplication::translate("text", "\350\275\257\344\273\266\345\267\245\347\250\213", nullptr));
        QTableWidgetItem *___qtablewidgetitem22 = tableWidget_2->item(0, 1);
        ___qtablewidgetitem22->setText(QCoreApplication::translate("text", "4.0", nullptr));
        QTableWidgetItem *___qtablewidgetitem23 = tableWidget_2->item(0, 2);
        ___qtablewidgetitem23->setText(QCoreApplication::translate("text", "\344\270\223\344\270\232\345\237\272\347\241\200\350\257\276/\345\277\205\344\277\256\350\257\276", nullptr));
        QTableWidgetItem *___qtablewidgetitem24 = tableWidget_2->item(0, 3);
        ___qtablewidgetitem24->setText(QCoreApplication::translate("text", "100", nullptr));
        QTableWidgetItem *___qtablewidgetitem25 = tableWidget_2->item(0, 4);
        ___qtablewidgetitem25->setText(QCoreApplication::translate("text", "4.0", nullptr));
        QTableWidgetItem *___qtablewidgetitem26 = tableWidget_2->item(1, 0);
        ___qtablewidgetitem26->setText(QCoreApplication::translate("text", "\347\225\214\351\235\242\344\272\244\344\272\222", nullptr));
        QTableWidgetItem *___qtablewidgetitem27 = tableWidget_2->item(1, 1);
        ___qtablewidgetitem27->setText(QCoreApplication::translate("text", "4.0", nullptr));
        QTableWidgetItem *___qtablewidgetitem28 = tableWidget_2->item(1, 2);
        ___qtablewidgetitem28->setText(QCoreApplication::translate("text", "\344\270\223\344\270\232\350\257\276/\351\231\220\351\200\211\350\257\276", nullptr));
        QTableWidgetItem *___qtablewidgetitem29 = tableWidget_2->item(1, 3);
        ___qtablewidgetitem29->setText(QCoreApplication::translate("text", "100", nullptr));
        QTableWidgetItem *___qtablewidgetitem30 = tableWidget_2->item(1, 4);
        ___qtablewidgetitem30->setText(QCoreApplication::translate("text", "4.0", nullptr));
        QTableWidgetItem *___qtablewidgetitem31 = tableWidget_2->item(2, 0);
        ___qtablewidgetitem31->setText(QCoreApplication::translate("text", "linux\346\223\215\344\275\234\347\263\273\347\273\237", nullptr));
        QTableWidgetItem *___qtablewidgetitem32 = tableWidget_2->item(2, 1);
        ___qtablewidgetitem32->setText(QCoreApplication::translate("text", "4.0", nullptr));
        QTableWidgetItem *___qtablewidgetitem33 = tableWidget_2->item(2, 2);
        ___qtablewidgetitem33->setText(QCoreApplication::translate("text", "\344\270\223\344\270\232\345\237\272\347\241\200\350\257\276/\345\277\205\344\277\256\350\257\276", nullptr));
        QTableWidgetItem *___qtablewidgetitem34 = tableWidget_2->item(2, 3);
        ___qtablewidgetitem34->setText(QCoreApplication::translate("text", "100", nullptr));
        QTableWidgetItem *___qtablewidgetitem35 = tableWidget_2->item(2, 4);
        ___qtablewidgetitem35->setText(QCoreApplication::translate("text", "4.0", nullptr));
        QTableWidgetItem *___qtablewidgetitem36 = tableWidget_2->item(3, 0);
        ___qtablewidgetitem36->setText(QCoreApplication::translate("text", "C++\347\250\213\345\272\217\350\256\276\350\256\241", nullptr));
        QTableWidgetItem *___qtablewidgetitem37 = tableWidget_2->item(3, 1);
        ___qtablewidgetitem37->setText(QCoreApplication::translate("text", "4.0", nullptr));
        QTableWidgetItem *___qtablewidgetitem38 = tableWidget_2->item(3, 2);
        ___qtablewidgetitem38->setText(QCoreApplication::translate("text", "\344\270\223\344\270\232\350\257\276/\351\231\220\351\200\211\350\257\276", nullptr));
        QTableWidgetItem *___qtablewidgetitem39 = tableWidget_2->item(3, 3);
        ___qtablewidgetitem39->setText(QCoreApplication::translate("text", "100", nullptr));
        QTableWidgetItem *___qtablewidgetitem40 = tableWidget_2->item(3, 4);
        ___qtablewidgetitem40->setText(QCoreApplication::translate("text", "4.0", nullptr));
        tableWidget_2->setSortingEnabled(__sortingEnabled1);

        label_8->setText(QCoreApplication::translate("text", "<html><head/><body><p>\345\255\246\345\271\264\345\255\246\346\234\237</p></body></html>", nullptr));
        comboBox_4->setItemText(0, QCoreApplication::translate("text", "2021-2022\345\255\246\345\271\264\347\254\254\344\270\200\345\255\246\346\234\237", nullptr));
        comboBox_4->setItemText(1, QCoreApplication::translate("text", "2021-2022\345\255\246\345\271\264\347\254\254\344\272\214\345\255\246\346\234\237", nullptr));
        comboBox_4->setItemText(2, QCoreApplication::translate("text", "2021-2022\345\255\246\345\271\264\347\254\254\344\270\211\345\255\246\346\234\237", nullptr));

        label_2->setText(QCoreApplication::translate("text", "<html><head/><body><p><span style=\" font-weight:700;\">\350\200\203\350\257\225\345\256\211\346\216\222</span></p></body></html>", nullptr));
        label_3->setText(QCoreApplication::translate("text", "\350\200\203\350\257\225\346\200\247\350\264\250", nullptr));
        comboBox_5->setItemText(0, QCoreApplication::translate("text", "\346\234\253\350\200\203", nullptr));
        comboBox_5->setItemText(1, QCoreApplication::translate("text", "\344\270\255\350\200\203", nullptr));
        comboBox_5->setItemText(2, QCoreApplication::translate("text", "\350\241\245\350\200\203", nullptr));

        pushButton_5->setText(QCoreApplication::translate("text", "\346\220\234\347\264\242", nullptr));
        QTableWidgetItem *___qtablewidgetitem41 = tableWidget_3->horizontalHeaderItem(0);
        ___qtablewidgetitem41->setText(QCoreApplication::translate("text", "\350\257\276\347\250\213", nullptr));
        QTableWidgetItem *___qtablewidgetitem42 = tableWidget_3->horizontalHeaderItem(1);
        ___qtablewidgetitem42->setText(QCoreApplication::translate("text", "\346\227\245\346\234\237", nullptr));
        QTableWidgetItem *___qtablewidgetitem43 = tableWidget_3->horizontalHeaderItem(2);
        ___qtablewidgetitem43->setText(QCoreApplication::translate("text", "\346\227\266\351\227\264", nullptr));
        QTableWidgetItem *___qtablewidgetitem44 = tableWidget_3->horizontalHeaderItem(3);
        ___qtablewidgetitem44->setText(QCoreApplication::translate("text", "\350\200\203\345\234\272", nullptr));

        const bool __sortingEnabled2 = tableWidget_3->isSortingEnabled();
        tableWidget_3->setSortingEnabled(false);
        QTableWidgetItem *___qtablewidgetitem45 = tableWidget_3->item(0, 0);
        ___qtablewidgetitem45->setText(QCoreApplication::translate("text", "\350\275\257\344\273\266\345\267\245\347\250\213", nullptr));
        QTableWidgetItem *___qtablewidgetitem46 = tableWidget_3->item(0, 1);
        ___qtablewidgetitem46->setText(QCoreApplication::translate("text", "5.30", nullptr));
        QTableWidgetItem *___qtablewidgetitem47 = tableWidget_3->item(0, 2);
        ___qtablewidgetitem47->setText(QCoreApplication::translate("text", "8:00-9:30", nullptr));
        QTableWidgetItem *___qtablewidgetitem48 = tableWidget_3->item(0, 3);
        ___qtablewidgetitem48->setText(QCoreApplication::translate("text", "A7-201", nullptr));
        QTableWidgetItem *___qtablewidgetitem49 = tableWidget_3->item(1, 0);
        ___qtablewidgetitem49->setText(QCoreApplication::translate("text", "\347\225\214\351\235\242\344\272\244\344\272\222", nullptr));
        QTableWidgetItem *___qtablewidgetitem50 = tableWidget_3->item(1, 1);
        ___qtablewidgetitem50->setText(QCoreApplication::translate("text", "5.30", nullptr));
        QTableWidgetItem *___qtablewidgetitem51 = tableWidget_3->item(1, 2);
        ___qtablewidgetitem51->setText(QCoreApplication::translate("text", "10:00-11:30", nullptr));
        QTableWidgetItem *___qtablewidgetitem52 = tableWidget_3->item(1, 3);
        ___qtablewidgetitem52->setText(QCoreApplication::translate("text", "A7-316", nullptr));
        QTableWidgetItem *___qtablewidgetitem53 = tableWidget_3->item(2, 0);
        ___qtablewidgetitem53->setText(QCoreApplication::translate("text", "linux\347\263\273\347\273\237", nullptr));
        QTableWidgetItem *___qtablewidgetitem54 = tableWidget_3->item(2, 1);
        ___qtablewidgetitem54->setText(QCoreApplication::translate("text", "5.31", nullptr));
        QTableWidgetItem *___qtablewidgetitem55 = tableWidget_3->item(2, 2);
        ___qtablewidgetitem55->setText(QCoreApplication::translate("text", "8:00-9:30", nullptr));
        QTableWidgetItem *___qtablewidgetitem56 = tableWidget_3->item(2, 3);
        ___qtablewidgetitem56->setText(QCoreApplication::translate("text", "A7-102", nullptr));
        QTableWidgetItem *___qtablewidgetitem57 = tableWidget_3->item(3, 0);
        ___qtablewidgetitem57->setText(QCoreApplication::translate("text", "C++\347\250\213\345\272\217\350\256\276\350\256\241", nullptr));
        QTableWidgetItem *___qtablewidgetitem58 = tableWidget_3->item(3, 1);
        ___qtablewidgetitem58->setText(QCoreApplication::translate("text", "5.31", nullptr));
        QTableWidgetItem *___qtablewidgetitem59 = tableWidget_3->item(3, 2);
        ___qtablewidgetitem59->setText(QCoreApplication::translate("text", "13:30-15:00", nullptr));
        QTableWidgetItem *___qtablewidgetitem60 = tableWidget_3->item(3, 3);
        ___qtablewidgetitem60->setText(QCoreApplication::translate("text", "A7-215", nullptr));
        tableWidget_3->setSortingEnabled(__sortingEnabled2);

    } // retranslateUi

};

namespace Ui {
    class text: public Ui_text {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TEXT_H
