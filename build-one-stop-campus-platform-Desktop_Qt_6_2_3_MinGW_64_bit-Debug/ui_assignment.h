/********************************************************************************
** Form generated from reading UI file 'assignment.ui'
**
** Created by: Qt User Interface Compiler version 6.2.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ASSIGNMENT_H
#define UI_ASSIGNMENT_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_assignment
{
public:
    QTextEdit *textEdit;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton;
    QSpacerItem *verticalSpacer;
    QStackedWidget *stackedWidget;
    QWidget *page;
    QWidget *page_2;
    QWidget *verticalLayoutWidget_2;
    QVBoxLayout *verticalLayout_2;
    QLabel *label;
    QTextEdit *textEdit_2;
    QRadioButton *radioButton;
    QRadioButton *radioButton_2;
    QRadioButton *radioButton_3;
    QPushButton *pushButton_4;
    QPushButton *pushButton_5;
    QPushButton *pushButton_6;
    QWidget *page_3;
    QWidget *verticalLayoutWidget_3;
    QVBoxLayout *verticalLayout_3;
    QLabel *label_2;
    QTextBrowser *textBrowser;
    QRadioButton *radioButton_4;
    QRadioButton *radioButton_5;
    QPushButton *pushButton_7;
    QPushButton *pushButton_8;
    QWidget *page_4;
    QWidget *verticalLayoutWidget_4;
    QVBoxLayout *verticalLayout_4;
    QLabel *label_3;
    QTextBrowser *textBrowser_2;
    QLabel *label_4;
    QTextBrowser *textBrowser_3;
    QPushButton *pushButton_9;

    void setupUi(QWidget *assignment)
    {
        if (assignment->objectName().isEmpty())
            assignment->setObjectName(QString::fromUtf8("assignment"));
        assignment->resize(1305, 546);
        textEdit = new QTextEdit(assignment);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));
        textEdit->setGeometry(QRect(0, 0, 1311, 51));
        verticalLayoutWidget = new QWidget(assignment);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(0, 70, 181, 421));
        verticalLayout = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        pushButton_2 = new QPushButton(verticalLayoutWidget);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        verticalLayout->addWidget(pushButton_2);

        pushButton_3 = new QPushButton(verticalLayoutWidget);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));

        verticalLayout->addWidget(pushButton_3);

        pushButton = new QPushButton(verticalLayoutWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        verticalLayout->addWidget(pushButton);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);

        stackedWidget = new QStackedWidget(assignment);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setGeometry(QRect(250, 70, 1021, 421));
        page = new QWidget();
        page->setObjectName(QString::fromUtf8("page"));
        stackedWidget->addWidget(page);
        page_2 = new QWidget();
        page_2->setObjectName(QString::fromUtf8("page_2"));
        verticalLayoutWidget_2 = new QWidget(page_2);
        verticalLayoutWidget_2->setObjectName(QString::fromUtf8("verticalLayoutWidget_2"));
        verticalLayoutWidget_2->setGeometry(QRect(30, 10, 961, 276));
        verticalLayout_2 = new QVBoxLayout(verticalLayoutWidget_2);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(0, 0, 0, 0);
        label = new QLabel(verticalLayoutWidget_2);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout_2->addWidget(label);

        textEdit_2 = new QTextEdit(verticalLayoutWidget_2);
        textEdit_2->setObjectName(QString::fromUtf8("textEdit_2"));

        verticalLayout_2->addWidget(textEdit_2);

        radioButton = new QRadioButton(verticalLayoutWidget_2);
        radioButton->setObjectName(QString::fromUtf8("radioButton"));

        verticalLayout_2->addWidget(radioButton);

        radioButton_2 = new QRadioButton(verticalLayoutWidget_2);
        radioButton_2->setObjectName(QString::fromUtf8("radioButton_2"));

        verticalLayout_2->addWidget(radioButton_2);

        radioButton_3 = new QRadioButton(verticalLayoutWidget_2);
        radioButton_3->setObjectName(QString::fromUtf8("radioButton_3"));

        verticalLayout_2->addWidget(radioButton_3);

        pushButton_4 = new QPushButton(verticalLayoutWidget_2);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));

        verticalLayout_2->addWidget(pushButton_4);

        pushButton_5 = new QPushButton(page_2);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));
        pushButton_5->setGeometry(QRect(780, 300, 92, 29));
        pushButton_6 = new QPushButton(page_2);
        pushButton_6->setObjectName(QString::fromUtf8("pushButton_6"));
        pushButton_6->setGeometry(QRect(890, 300, 92, 29));
        stackedWidget->addWidget(page_2);
        page_3 = new QWidget();
        page_3->setObjectName(QString::fromUtf8("page_3"));
        verticalLayoutWidget_3 = new QWidget(page_3);
        verticalLayoutWidget_3->setObjectName(QString::fromUtf8("verticalLayoutWidget_3"));
        verticalLayoutWidget_3->setGeometry(QRect(50, 30, 921, 261));
        verticalLayout_3 = new QVBoxLayout(verticalLayoutWidget_3);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        label_2 = new QLabel(verticalLayoutWidget_3);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        verticalLayout_3->addWidget(label_2);

        textBrowser = new QTextBrowser(verticalLayoutWidget_3);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));

        verticalLayout_3->addWidget(textBrowser);

        radioButton_4 = new QRadioButton(verticalLayoutWidget_3);
        radioButton_4->setObjectName(QString::fromUtf8("radioButton_4"));

        verticalLayout_3->addWidget(radioButton_4);

        radioButton_5 = new QRadioButton(verticalLayoutWidget_3);
        radioButton_5->setObjectName(QString::fromUtf8("radioButton_5"));

        verticalLayout_3->addWidget(radioButton_5);

        pushButton_7 = new QPushButton(page_3);
        pushButton_7->setObjectName(QString::fromUtf8("pushButton_7"));
        pushButton_7->setGeometry(QRect(760, 310, 92, 29));
        pushButton_8 = new QPushButton(page_3);
        pushButton_8->setObjectName(QString::fromUtf8("pushButton_8"));
        pushButton_8->setGeometry(QRect(870, 310, 92, 29));
        stackedWidget->addWidget(page_3);
        page_4 = new QWidget();
        page_4->setObjectName(QString::fromUtf8("page_4"));
        verticalLayoutWidget_4 = new QWidget(page_4);
        verticalLayoutWidget_4->setObjectName(QString::fromUtf8("verticalLayoutWidget_4"));
        verticalLayoutWidget_4->setGeometry(QRect(70, 20, 911, 311));
        verticalLayout_4 = new QVBoxLayout(verticalLayoutWidget_4);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        label_3 = new QLabel(verticalLayoutWidget_4);
        label_3->setObjectName(QString::fromUtf8("label_3"));

        verticalLayout_4->addWidget(label_3);

        textBrowser_2 = new QTextBrowser(verticalLayoutWidget_4);
        textBrowser_2->setObjectName(QString::fromUtf8("textBrowser_2"));

        verticalLayout_4->addWidget(textBrowser_2);

        label_4 = new QLabel(verticalLayoutWidget_4);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        verticalLayout_4->addWidget(label_4);

        textBrowser_3 = new QTextBrowser(verticalLayoutWidget_4);
        textBrowser_3->setObjectName(QString::fromUtf8("textBrowser_3"));

        verticalLayout_4->addWidget(textBrowser_3);

        pushButton_9 = new QPushButton(verticalLayoutWidget_4);
        pushButton_9->setObjectName(QString::fromUtf8("pushButton_9"));

        verticalLayout_4->addWidget(pushButton_9);

        stackedWidget->addWidget(page_4);

        retranslateUi(assignment);

        stackedWidget->setCurrentIndex(3);


        QMetaObject::connectSlotsByName(assignment);
    } // setupUi

    void retranslateUi(QWidget *assignment)
    {
        assignment->setWindowTitle(QCoreApplication::translate("assignment", "Form", nullptr));
        textEdit->setHtml(QCoreApplication::translate("assignment", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><meta charset=\"utf-8\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Microsoft YaHei UI'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:14pt; font-weight:700;\">\347\274\226\350\276\221\344\275\234\344\270\232</span></p></body></html>", nullptr));
        pushButton_2->setText(QCoreApplication::translate("assignment", "\351\200\211\346\213\251\351\242\230", nullptr));
        pushButton_3->setText(QCoreApplication::translate("assignment", "\345\210\244\346\226\255\351\242\230", nullptr));
        pushButton->setText(QCoreApplication::translate("assignment", "\347\256\200\347\255\224\351\242\230", nullptr));
        label->setText(QCoreApplication::translate("assignment", "\351\227\256\351\242\2301", nullptr));
        radioButton->setText(QCoreApplication::translate("assignment", "A", nullptr));
        radioButton_2->setText(QCoreApplication::translate("assignment", "B", nullptr));
        radioButton_3->setText(QCoreApplication::translate("assignment", "C", nullptr));
        pushButton_4->setText(QCoreApplication::translate("assignment", "\345\242\236\345\212\240\351\200\211\351\241\271", nullptr));
        pushButton_5->setText(QCoreApplication::translate("assignment", "\346\267\273\345\212\240\351\227\256\351\242\230", nullptr));
        pushButton_6->setText(QCoreApplication::translate("assignment", "\344\270\212\344\274\240", nullptr));
        label_2->setText(QCoreApplication::translate("assignment", "\351\227\256\351\242\2301", nullptr));
        radioButton_4->setText(QCoreApplication::translate("assignment", "T", nullptr));
        radioButton_5->setText(QCoreApplication::translate("assignment", "F", nullptr));
        pushButton_7->setText(QCoreApplication::translate("assignment", "\346\267\273\345\212\240\351\227\256\351\242\230", nullptr));
        pushButton_8->setText(QCoreApplication::translate("assignment", "\344\270\212\344\274\240", nullptr));
        label_3->setText(QCoreApplication::translate("assignment", "\351\227\256\351\242\2301", nullptr));
        label_4->setText(QCoreApplication::translate("assignment", "\347\255\224\346\241\210", nullptr));
        pushButton_9->setText(QCoreApplication::translate("assignment", "\344\270\212\344\274\240\351\231\204\344\273\266", nullptr));
    } // retranslateUi

};

namespace Ui {
    class assignment: public Ui_assignment {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ASSIGNMENT_H
