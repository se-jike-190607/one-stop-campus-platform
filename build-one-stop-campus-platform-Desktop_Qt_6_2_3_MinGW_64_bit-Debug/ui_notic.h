/********************************************************************************
** Form generated from reading UI file 'notic.ui'
**
** Created by: Qt User Interface Compiler version 6.2.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NOTIC_H
#define UI_NOTIC_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_notic
{
public:
    QTextBrowser *textBrowser;
    QTextEdit *textEdit;
    QPushButton *pushButton;
    QPushButton *pushButton_2;

    void setupUi(QWidget *notic)
    {
        if (notic->objectName().isEmpty())
            notic->setObjectName(QString::fromUtf8("notic"));
        notic->setEnabled(true);
        notic->resize(1102, 549);
        textBrowser = new QTextBrowser(notic);
        textBrowser->setObjectName(QString::fromUtf8("textBrowser"));
        textBrowser->setGeometry(QRect(0, 0, 1101, 41));
        textEdit = new QTextEdit(notic);
        textEdit->setObjectName(QString::fromUtf8("textEdit"));
        textEdit->setGeometry(QRect(110, 60, 851, 351));
        pushButton = new QPushButton(notic);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(110, 420, 851, 29));
        pushButton_2 = new QPushButton(notic);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setGeometry(QRect(870, 460, 92, 29));

        retranslateUi(notic);

        QMetaObject::connectSlotsByName(notic);
    } // setupUi

    void retranslateUi(QWidget *notic)
    {
        notic->setWindowTitle(QCoreApplication::translate("notic", "Form", nullptr));
        textBrowser->setHtml(QCoreApplication::translate("notic", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><meta charset=\"utf-8\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Microsoft YaHei UI'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:14pt; font-weight:700;\">\345\205\254\345\221\212</span></p></body></html>", nullptr));
        pushButton->setText(QCoreApplication::translate("notic", "\344\270\212\344\274\240\351\231\204\344\273\266", nullptr));
        pushButton_2->setText(QCoreApplication::translate("notic", "\344\270\212\344\274\240", nullptr));
    } // retranslateUi

};

namespace Ui {
    class notic: public Ui_notic {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NOTIC_H
