/********************************************************************************
** Form generated from reading UI file 'form.ui'
**
** Created by: Qt User Interface Compiler version 6.2.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FORM_H
#define UI_FORM_H

#include <QtCore/QVariant>
#include <QtGui/QIcon>
#include <QtWidgets/QAbstractButton>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCalendarWidget>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStackedWidget>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Form
{
public:
    QWidget *widget_4;
    QFrame *data;
    QVBoxLayout *verticalLayout_2;
    QSpinBox *spinBox_2;
    QSpacerItem *verticalSpacer_2;
    QSpinBox *spinBox;
    QSpacerItem *verticalSpacer_3;
    QCalendarWidget *calendarWidget;
    QListWidget *listWidget;
    QWidget *widget_2;
    QVBoxLayout *verticalLayout_3;
    QToolButton *toolButton;
    QToolButton *toolButton_2;
    QToolButton *toolButton_3;
    QListWidget *listWidget_2;
    QWidget *widget_5;
    QToolButton *toolButton_4;
    QToolButton *toolButton_5;
    QToolButton *toolButton_6;
    QLabel *label_5;
    QStackedWidget *stackedWidget;
    QWidget *page_5;
    QWidget *Login;
    QLabel *label_3;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QWidget *user;
    QHBoxLayout *horizontalLayout;
    QLabel *label;
    QLineEdit *lineEdit;
    QWidget *widget;
    QHBoxLayout *horizontalLayout_2;
    QLabel *label_2;
    QLineEdit *lineEdit_2;
    QSpacerItem *verticalSpacer;
    QDialogButtonBox *buttonBox;
    QWidget *page_7;
    QPushButton *pushButton_2;
    QWidget *page_6;
    QWidget *verticalLayoutWidget;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_6;
    QLabel *label_7;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_9;
    QLabel *label_8;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_11;
    QLabel *label_10;
    QHBoxLayout *horizontalLayout_9;
    QSpacerItem *horizontalSpacer_5;
    QPushButton *pushButton;

    void setupUi(QWidget *Form)
    {
        if (Form->objectName().isEmpty())
            Form->setObjectName(QString::fromUtf8("Form"));
        Form->resize(1300, 737);
        widget_4 = new QWidget(Form);
        widget_4->setObjectName(QString::fromUtf8("widget_4"));
        widget_4->setGeometry(QRect(10, 10, 1271, 701));
        data = new QFrame(widget_4);
        data->setObjectName(QString::fromUtf8("data"));
        data->setGeometry(QRect(940, 10, 320, 351));
        data->setFrameShape(QFrame::StyledPanel);
        data->setFrameShadow(QFrame::Raised);
        verticalLayout_2 = new QVBoxLayout(data);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        spinBox_2 = new QSpinBox(data);
        spinBox_2->setObjectName(QString::fromUtf8("spinBox_2"));

        verticalLayout_2->addWidget(spinBox_2);

        verticalSpacer_2 = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_2->addItem(verticalSpacer_2);

        spinBox = new QSpinBox(data);
        spinBox->setObjectName(QString::fromUtf8("spinBox"));

        verticalLayout_2->addWidget(spinBox);

        verticalSpacer_3 = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_2->addItem(verticalSpacer_3);

        calendarWidget = new QCalendarWidget(data);
        calendarWidget->setObjectName(QString::fromUtf8("calendarWidget"));

        verticalLayout_2->addWidget(calendarWidget);

        listWidget = new QListWidget(widget_4);
        QFont font;
        font.setBold(true);
        font.setKerning(true);
        QListWidgetItem *__qlistwidgetitem = new QListWidgetItem(listWidget);
        __qlistwidgetitem->setFont(font);
        listWidget->setObjectName(QString::fromUtf8("listWidget"));
        listWidget->setGeometry(QRect(390, 20, 511, 301));
        widget_2 = new QWidget(widget_4);
        widget_2->setObjectName(QString::fromUtf8("widget_2"));
        widget_2->setGeometry(QRect(940, 370, 311, 321));
        verticalLayout_3 = new QVBoxLayout(widget_2);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        toolButton = new QToolButton(widget_2);
        toolButton->setObjectName(QString::fromUtf8("toolButton"));
        toolButton->setToolTipDuration(-1);
        toolButton->setStyleSheet(QString::fromUtf8(""));
        QIcon icon;
        icon.addFile(QString::fromUtf8("C:/Users/h'p/Pictures/\345\276\256\344\277\241\345\233\276\347\211\207_20200910143409.jpg"), QSize(), QIcon::Normal, QIcon::Off);
        toolButton->setIcon(icon);
        toolButton->setIconSize(QSize(100, 100));
        toolButton->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
        toolButton->setAutoRaise(true);
        toolButton->setArrowType(Qt::NoArrow);

        verticalLayout_3->addWidget(toolButton);

        toolButton_2 = new QToolButton(widget_2);
        toolButton_2->setObjectName(QString::fromUtf8("toolButton_2"));
        QIcon icon1;
        icon1.addFile(QString::fromUtf8("C:/Users/h'p/Pictures/v2-cf8518d6c4a42def827ab46997e0f0a9_r.jpg"), QSize(), QIcon::Normal, QIcon::Off);
        toolButton_2->setIcon(icon1);
        toolButton_2->setIconSize(QSize(100, 120));
        toolButton_2->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
        toolButton_2->setAutoRaise(true);

        verticalLayout_3->addWidget(toolButton_2);

        toolButton_3 = new QToolButton(widget_2);
        toolButton_3->setObjectName(QString::fromUtf8("toolButton_3"));
        QIcon icon2;
        icon2.addFile(QString::fromUtf8("C:/Users/h'p/Pictures/v2-ec1a40f246b7d95c59774482141b2d44_r.jpg"), QSize(), QIcon::Normal, QIcon::Off);
        toolButton_3->setIcon(icon2);
        toolButton_3->setIconSize(QSize(100, 100));
        toolButton_3->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
        toolButton_3->setAutoRaise(true);

        verticalLayout_3->addWidget(toolButton_3);

        listWidget_2 = new QListWidget(widget_4);
        QFont font1;
        font1.setBold(true);
        QListWidgetItem *__qlistwidgetitem1 = new QListWidgetItem(listWidget_2);
        __qlistwidgetitem1->setFont(font1);
        listWidget_2->setObjectName(QString::fromUtf8("listWidget_2"));
        listWidget_2->setGeometry(QRect(390, 350, 511, 321));
        widget_5 = new QWidget(widget_4);
        widget_5->setObjectName(QString::fromUtf8("widget_5"));
        widget_5->setGeometry(QRect(20, 350, 341, 331));
        toolButton_4 = new QToolButton(widget_5);
        toolButton_4->setObjectName(QString::fromUtf8("toolButton_4"));
        toolButton_4->setGeometry(QRect(30, 130, 241, 41));
        toolButton_5 = new QToolButton(widget_5);
        toolButton_5->setObjectName(QString::fromUtf8("toolButton_5"));
        toolButton_5->setGeometry(QRect(30, 80, 241, 41));
        toolButton_6 = new QToolButton(widget_5);
        toolButton_6->setObjectName(QString::fromUtf8("toolButton_6"));
        toolButton_6->setGeometry(QRect(30, 180, 241, 41));
        label_5 = new QLabel(widget_5);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(30, 20, 69, 20));
        stackedWidget = new QStackedWidget(widget_4);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setEnabled(true);
        stackedWidget->setGeometry(QRect(10, 10, 371, 321));
        stackedWidget->setFrameShadow(QFrame::Plain);
        page_5 = new QWidget();
        page_5->setObjectName(QString::fromUtf8("page_5"));
        Login = new QWidget(page_5);
        Login->setObjectName(QString::fromUtf8("Login"));
        Login->setGeometry(QRect(0, 10, 351, 311));
        label_3 = new QLabel(Login);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(130, 0, 71, 41));
        label_3->setAcceptDrops(false);
        layoutWidget = new QWidget(Login);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(30, 50, 312, 241));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        user = new QWidget(layoutWidget);
        user->setObjectName(QString::fromUtf8("user"));
        horizontalLayout = new QHBoxLayout(user);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        label = new QLabel(user);
        label->setObjectName(QString::fromUtf8("label"));

        horizontalLayout->addWidget(label);

        lineEdit = new QLineEdit(user);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));

        horizontalLayout->addWidget(lineEdit);


        verticalLayout->addWidget(user);

        widget = new QWidget(layoutWidget);
        widget->setObjectName(QString::fromUtf8("widget"));
        horizontalLayout_2 = new QHBoxLayout(widget);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        label_2 = new QLabel(widget);
        label_2->setObjectName(QString::fromUtf8("label_2"));

        horizontalLayout_2->addWidget(label_2);

        lineEdit_2 = new QLineEdit(widget);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        lineEdit_2->setEchoMode(QLineEdit::Password);

        horizontalLayout_2->addWidget(lineEdit_2);


        verticalLayout->addWidget(widget);

        verticalSpacer = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout->addItem(verticalSpacer);

        buttonBox = new QDialogButtonBox(layoutWidget);
        buttonBox->setObjectName(QString::fromUtf8("buttonBox"));
        buttonBox->setStandardButtons(QDialogButtonBox::Cancel|QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);

        stackedWidget->addWidget(page_5);
        page_7 = new QWidget();
        page_7->setObjectName(QString::fromUtf8("page_7"));
        pushButton_2 = new QPushButton(page_7);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setGeometry(QRect(100, 150, 93, 29));
        stackedWidget->addWidget(page_7);
        page_6 = new QWidget();
        page_6->setObjectName(QString::fromUtf8("page_6"));
        verticalLayoutWidget = new QWidget(page_6);
        verticalLayoutWidget->setObjectName(QString::fromUtf8("verticalLayoutWidget"));
        verticalLayoutWidget->setGeometry(QRect(9, 9, 345, 301));
        verticalLayout_4 = new QVBoxLayout(verticalLayoutWidget);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QString::fromUtf8("horizontalLayout_5"));
        label_6 = new QLabel(verticalLayoutWidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setLayoutDirection(Qt::RightToLeft);
        label_6->setAlignment(Qt::AlignCenter);

        horizontalLayout_5->addWidget(label_6);

        label_7 = new QLabel(verticalLayoutWidget);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setEnabled(true);
        label_7->setMinimumSize(QSize(200, 0));

        horizontalLayout_5->addWidget(label_7);


        verticalLayout_4->addLayout(horizontalLayout_5);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_9 = new QLabel(verticalLayoutWidget);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setAlignment(Qt::AlignCenter);

        horizontalLayout_7->addWidget(label_9);

        label_8 = new QLabel(verticalLayoutWidget);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setMinimumSize(QSize(200, 0));

        horizontalLayout_7->addWidget(label_8);


        verticalLayout_4->addLayout(horizontalLayout_7);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_11 = new QLabel(verticalLayoutWidget);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setAlignment(Qt::AlignCenter);

        horizontalLayout_8->addWidget(label_11);

        label_10 = new QLabel(verticalLayoutWidget);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setMinimumSize(QSize(200, 0));

        horizontalLayout_8->addWidget(label_10);


        verticalLayout_4->addLayout(horizontalLayout_8);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_9->addItem(horizontalSpacer_5);

        pushButton = new QPushButton(verticalLayoutWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        horizontalLayout_9->addWidget(pushButton);


        verticalLayout_4->addLayout(horizontalLayout_9);

        stackedWidget->addWidget(page_6);

        retranslateUi(Form);

        stackedWidget->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(Form);
    } // setupUi

    void retranslateUi(QWidget *Form)
    {
        Form->setWindowTitle(QCoreApplication::translate("Form", "Form", nullptr));

        const bool __sortingEnabled = listWidget->isSortingEnabled();
        listWidget->setSortingEnabled(false);
        QListWidgetItem *___qlistwidgetitem = listWidget->item(0);
        ___qlistwidgetitem->setText(QCoreApplication::translate("Form", "\351\200\232\347\237\245\345\205\254\345\221\212", nullptr));
        listWidget->setSortingEnabled(__sortingEnabled);

        toolButton->setText(QCoreApplication::translate("Form", "\344\275\234\346\201\257\346\227\266\351\227\264", nullptr));
        toolButton_2->setText(QCoreApplication::translate("Form", "\346\237\245\347\234\213\346\240\241\345\216\206", nullptr));
        toolButton_3->setText(QCoreApplication::translate("Form", "\347\256\241\347\220\206\344\272\272\345\221\230", nullptr));

        const bool __sortingEnabled1 = listWidget_2->isSortingEnabled();
        listWidget_2->setSortingEnabled(false);
        QListWidgetItem *___qlistwidgetitem1 = listWidget_2->item(0);
        ___qlistwidgetitem1->setText(QCoreApplication::translate("Form", "\345\205\254\345\205\261\344\270\213\350\275\275", nullptr));
        listWidget_2->setSortingEnabled(__sortingEnabled1);

        toolButton_4->setText(QCoreApplication::translate("Form", "...", nullptr));
        toolButton_5->setText(QCoreApplication::translate("Form", "...", nullptr));
        toolButton_6->setText(QCoreApplication::translate("Form", "...", nullptr));
        label_5->setText(QCoreApplication::translate("Form", "\346\225\231\345\255\246\345\256\211\346\216\222", nullptr));
        label_3->setText(QCoreApplication::translate("Form", "\350\264\246\345\217\267\347\231\273\345\275\225", nullptr));
        label->setText(QCoreApplication::translate("Form", "\347\224\250\346\210\267\345\220\215\357\274\232", nullptr));
        label_2->setText(QCoreApplication::translate("Form", "   \345\257\206\347\240\201\357\274\232", nullptr));
        pushButton_2->setText(QCoreApplication::translate("Form", "PushButton", nullptr));
        label_6->setText(QCoreApplication::translate("Form", "\345\247\223\345\220\215:", nullptr));
        label_7->setText(QCoreApplication::translate("Form", "\347\250\213\346\231\272", nullptr));
        label_9->setText(QCoreApplication::translate("Form", "\345\255\246\345\217\267:", nullptr));
        label_8->setText(QCoreApplication::translate("Form", "19001010620", nullptr));
        label_11->setText(QCoreApplication::translate("Form", "\345\205\245\345\255\246\346\227\266\351\227\264:", nullptr));
        label_10->setText(QCoreApplication::translate("Form", "2019/9/1", nullptr));
        pushButton->setText(QCoreApplication::translate("Form", "\351\200\200\345\207\272\347\231\273\345\275\225", nullptr));
    } // retranslateUi

};

namespace Ui {
    class Form: public Ui_Form {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FORM_H
