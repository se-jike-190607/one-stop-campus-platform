CREATE DATABASE campus_platform;

use campus_platform;

CREATE TABLE IF NOT EXISTS `user`(
    `id` VARCHAR(20) NOT NULL,
    `password` VARCHAR(30) NOT NULL,
    `value` INT(3),
    `name` VARCHAR(15),
    `id_number` VARCHAR(20),
    `phone_number` VARCHAR(15),
    `sex` bit,
    `time_in_job` DATE,
    PRIMARY KEY ( `id` )
)ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS `course`(
    `id` VARCHAR(20) NOT NULL,
    `name` VARCHAR(15),
    `time_in_job` DATE,
    `teacher_id` VARCHAR(20),
    `flag` INT(7),
    FOREIGN KEY (`teacher_id`) REFERENCES user(`id`),
    PRIMARY KEY ( `id` )
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `assignment`(
    `id` VARCHAR(20) NOT NULL,
    `name` VARCHAR(15),
    `value` INT(3),
    `content` LONGTEXT,
    `course_id` VARCHAR(20),
    FOREIGN KEY (`course_id`) REFERENCES course(`id`),
    PRIMARY KEY ( `id` )
)ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS `score`(
    `student_id` VARCHAR(20) NOT NULL,
    `course_id` VARCHAR(20) NOT NULL,
    `common_score` INT(3),
    `end_score` INT(3),
    FOREIGN KEY (`student_id`) REFERENCES user(`id`),
    FOREIGN KEY (`course_id`) REFERENCES course(`id`),
    PRIMARY KEY ( `student_id`,`course_id` )
)ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE IF NOT EXISTS `announcement`(
    `id` VARCHAR(20) NOT NULL,
    `admin_id` VARCHAR(20) NOT NULL,
    `content` LONGTEXT,
    `accessory` TEXT,
    FOREIGN KEY (`admin_id`) REFERENCES user(`id`),
    PRIMARY KEY ( `id` )
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `ann_list`(
    `id` VARCHAR(20) NOT NULL,
    `user_id` VARCHAR(20) NOT NULL,
    `result` bit,
    FOREIGN KEY (`id`) REFERENCES announcement(`id`),
    FOREIGN KEY (`user_id`) REFERENCES user(`id`),
    PRIMARY KEY ( `id` ,`user_id`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;


